############### Question 2 :

DES> /consult jalousie.dl

Info: 9 rules consulted.

############### Question 3 :

DES> /listing.

aime(lapin,mon_chou).
aime(marcellus,mia).
aime(mon_chou,lapin).
aime(vincent,mia).
aime(vincent,pierre).
femme(jody).
femme(mia).
femme(yolande).
jaloux(X,Y) :-
  aime(X,Z),
  aime(Y,Z).

Info: 9 rules listed.

DES> /listing femme.

femme(jody).
femme(mia).
femme(yolande).

Info: 3 rules listed.

############### Question 4 :

DES> femme(mia).

{                                           
  femme(mia)
}
Info: 1 tuple computed.          

DES> femme(lapin).

{                                           
}
Info: 0 tuples computed. 

DES> femme(yolande).

{                                           
  femme(yolande)
}
Info: 1 tuple computed.  

############### Question 5 :

DES> femme(X).

{                                           
  femme(jody),
  femme(mia),
  femme(yolande)
}
Info: 3 tuples computed.  

############### Question 6 :

DES> femme(X),aime(vincent,X).

Info: Processing:
  answer(X) :-
    femme(X),
    aime(vincent,X).
{                                           
  answer(mia)
}
Info: 1 tuple computed. 

############### Question 7 :

DES> aime(vincent,X),not(femme(X)).

Info: Processing:
  answer(X) :-
    aime(vincent,X),
    not(femme(X)).
{                                           
  answer(pierre)
}
Info: 1 tuple computed.  

A et B ne sont pas commutatif. Ecrire not(femme(X)) provoque une erreur d'infinité. Une façon d'y remédier est d'écrire un nouveau prédicat homme(pierre).

############### Question 8 :

DES> jaloux(vincent,X).

{                                           
  jaloux(vincent,marcellus),
  jaloux(vincent,vincent)
}
Info: 2 tuples computed.  

############### Question 9 :

DES> jaloux(vincent,X),not(X=vincent).

Info: Processing:
  answer(X) :-
    jaloux(vincent,X),
    X\=vincent.
{                                           
  answer(marcellus)
}
Info: 1 tuple computed.

Sinon, plus simplement, modifier le prédicat jaloux par : jaloux(X, Y) :- aime(X, Z), aime(Y, Z), not(X=Y).

Ce qui nous donne :

DES> jaloux(vincent,X).

{                                           
  jaloux(vincent,marcellus)
}
Info: 1 tuple computed. 

############### Question 10 :

DES> jaloux(vincent,_).

Info: Processing:
  answer :-
    jaloux(vincent,_).
{                                           
  answer
}
Info: 1 tuple computed.    

Oui, Vincent est jaloux car il existe un tuple.
Le prédicat '_' fournit des tuples anonymes.







