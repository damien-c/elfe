######## Question 10.1 ########

DES> content(D).

{                                           
  content(timoleon)
}
Info: 1 tuple computed. 

######## Question 10.2 ########

DES> devrait_frequenter(timoleon, omnia).

{                                           
  devrait_frequenter(timoleon,omnia)
}
Info: 1 tuple computed.          

DES> devrait_frequenter(timoleon, taverneflamande).

{                                           
}
Info: 0 tuples computed. 

######## Question 10.3 ########

DES> triste(timoleon).

{                                           
}
Info: 0 tuples computed.          

DES> triste(foo).

{                                           
  triste(foo)
}
Info: 1 tuple computed.
